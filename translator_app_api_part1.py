import requests
import time

# Set User-defined function "def" with function name "azure..." and finally the function parameter, "(text)".
# Stored API key and region within underlining variables, so they can later be called in other functions. Mainly the endpoint and headers that Microsoft uses to authenticate us.

def azure_translate(text):
    API_KEY = "d492b98f55174c10b938b648242c3a64"
    API_REGION = "eastus"

    endpoint = "https://api.cognitive.microsofttranslator.com/"
    headers = {'Ocp-Apim-Subscription-Key': API_KEY,
        'Content-type': 'Application/json',
        'Ocp-Apim-Subscription-Region': API_REGION}
    params = {'api-version': '3.0', 'from': 'en', 'to': 'de'}
    api_url = endpoint+'translate'
    body = [{'text':text}]

    # Send a request method type of "POST" to Azure Translate API.
    response = requests.post(api_url, params=params, headers=headers, json=body)
    return response.json()


# Check our code with a simple main function...


if __name__=='__main__':
    translation = input("Please enter the word or word's that you wish to be translated into Deutsch: ")
    print("This is what you have chosen to be translated: ",translation)
    print("TRANSLATING")
    time.sleep(2)
    print(azure_translate(translation))

