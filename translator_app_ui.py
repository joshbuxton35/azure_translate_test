import ipywidgets as widgets

def on__button__clicked(b):
    response = azure_translate(w_text.value)
    w_output.clear_output()
    with w_output:
        print(response[0]['translations'][0]['text'])

w_header = widgets.HTML('<h2><i>SIMPLE</i><b>Translator</b></h2>')
w_text = widgets.Textarea(placeholder='Write something!', layout=widgets.Layout(width = '80%'))
w_button = widgets.Button(description='Translate To German')
w_button.on_click(on_button_clicked)
w_output = widgets.Output()
w_ui = widgets.VBox([w_header, w_text, w_button, w_output], layout=widgets.Layout(aligh_items='center'))
display(w_ui)
